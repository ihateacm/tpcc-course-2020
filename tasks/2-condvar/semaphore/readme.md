# Считающий семафор

Семафор – это автомат с жетонами, который поддерживает две операции:

- `Acquire()` – Взять один жетон из автомата. Если автомат пуст, то заблокироваться до появления в нем жетонов.
- `Release()` – Положить (вернуть) в автомат один жетон.
 
Начальное число жетонов в семафоре задается в конструкторе.

Жетон дает потоку, который его получил, право доступа к некоторому ограниченному пулу ресурсов.

Будем считать, что в семафор помещается неограниченное количество жетонов.

Семафор, конечно же, никакие жетоны явно не хранит, а просто поддерживает счетчик.

Другая интерпретация:

Семафор – это атомарный счетчик с операциями инкремента и декремента. При попытке декрементировать нулевой счетчик поток блокируется до тех пор, пока другой поток не поднимет значение выше нуля.

## Канал

Семафор – простой, но при этом выразительный примитив синхронизации. 

- [Semaphores are Surprisingly Versatile](https://preshing.com/20150316/semaphores-are-surprisingly-versatile/)
- [Little book of semaphores](http://greenteapress.com/semaphores/LittleBookOfSemaphores.pdf)

В этой задаче вы должны с помощью семафоров реализовать [канал](https://tour.golang.org/concurrency/3) для передачи данных между потоками.

## Задание

1) Реализуйте считающий семафор неограниченной емкости с помощью условных переменных.

2) С помощью семафоров реализуйте канал.

При реализации канала для синхронизации потоков допускается использовать _только_ семафоры. Другие примитивы (атомики, мьютексы, кондвары) использовать нельзя.

Шаблон решения – файлы `semaphore.hpp` и `channel.hpp`.
