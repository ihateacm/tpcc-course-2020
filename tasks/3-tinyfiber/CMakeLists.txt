cmake_minimum_required(VERSION 3.9)

add_subdirectory(coroutine)
add_subdirectory(echo)
add_subdirectory(sleep)
add_subdirectory(sleep-asio)
